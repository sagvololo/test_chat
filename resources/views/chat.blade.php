@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-heading">Чат</div>
            <div class="panel panel-default chat_border">

                <div class="panel-body">
                    <chat-messages :messages="messages"></chat-messages>
                </div>
            </div>
                <div class="panel-footer">
                    <chat-form
                        v-on:messagesent="addMessage"
                        :user="{{ Auth::user() }}"
                    ></chat-form>
                </div>
        </div>
    </div>
</div>
@endsection